
# SchemDraw Gallery


## Charging capacitor

![](gallery/cap_charge.png)

[Source](gallery/cap_charge.py)


-----------------------------------------------------------
## Loop Currents

![](gallery/loop_current.png)

[Source](gallery/loop_current.py)


-----------------------------------------------------------
## Operational Amplifier

![](gallery/opamp.png)

[Source](gallery/opamp.py)


-----------------------------------------------------------
## Source-Measure-Unit (SMU)

This example demonstrates grouping elements into a unit. 
Each SMU is grouped into a unit which is then placed where needed.

![](gallery/smu.png)

[Source](gallery/smu.py)


-----------------------------------------------------------
## SR Latch

![](gallery/SR.png)

[Source](gallery/SR.py)


-----------------------------------------------------------
## Resistor circle

Shows creation of elements within for loop, angled elements with automatic label positioning, and colors.

![](gallery/Rcircle.png)

[Source](gallery/Rcircle.py)



-----------------------------------------------------------
## Power Supply

![](gallery/powersupply.png)

[Source](gallery/powersupply.py)



-----------------------------------------------------------
## 741 Op-Amp

Internal schematic of a 741 IC.

![](gallery/741.png)

[Source](gallery/741.py)


-----------------------------------------------------------
## XKCD Mode

Turn on Matplotlib's XKCD mode for some hand-drawn looks!

![](gallery/ex_xkcd.png)

[Source](gallery/ex_xkcd.py)

![](gallery/SRxkcd.png)

[Source](gallery/SRxkcd.py)

